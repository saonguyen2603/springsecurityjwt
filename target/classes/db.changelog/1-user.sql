CREATE TABLE user_table
(
    "id"         bigserial PRIMARY KEY,
    "username"   varchar NOT NULL,
    "password"   varchar,
    "created_at" TIMESTAMP WITH TIME ZONE,
    "updated_at" TIMESTAMP WITH TIME ZONE,
    "created_by" integer,
    "updated_by" integer,
    "deleted_by" integer,
    "deleted_at" TIMESTAMP WITH TIME ZONE
);